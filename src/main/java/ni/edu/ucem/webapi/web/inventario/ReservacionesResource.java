/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.web.inventario;

import java.util.Date;
import javax.validation.Valid;
import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.core.ListApiResponse;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.service.ReservacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mzrodriguez
 */
@RestController
@RequestMapping("/v1/reservaciones")
public class ReservacionesResource {
    
     private final ReservacionService oReservacionService;
    
    @Autowired
    public ReservacionesResource(final ReservacionService oReservacionService)
    {
        this.oReservacionService = oReservacionService;
    }
    
    
    @RequestMapping(method = RequestMethod.GET, produces="application/json")
    public ListApiResponse<Reservacion> obtenerReservaciones(
            @RequestParam(value = "offset", required = false, defaultValue ="0") final Integer offset,
            @RequestParam(value = "limit", required = false, defaultValue="0") final Integer limit)
    {
        final Filtro paginacion = new Filtro.Builder()
                .paginacion(offset, limit)
                .build();

        Pagina<Reservacion> reservacion;
        reservacion = this.oReservacionService.obtenerTodosReservacion(paginacion);
        
        return new ListApiResponse<Reservacion>(ApiResponse.Status.OK, reservacion);
    }
    
     @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces="application/json")
    public ApiResponse obtener(@PathVariable("id") final int id)
    {
        final Reservacion reservacion = this.oReservacionService.obtenerReservacion(id);
        return new ApiResponse(ApiResponse.Status.OK, reservacion);
    }
    
    /** 
     * @param reservacion
     * @param result
     * @return
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(method = RequestMethod.POST,
            produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponse guardarReservacion(@Valid @RequestBody final Reservacion reservacion, BindingResult result) 
    {
        if(result.hasErrors())
        {
            throw new IllegalArgumentException(result.getFieldError().getDefaultMessage());
        }
        
        this.oReservacionService.agregarReservacion(reservacion);
        return new ApiResponse(ApiResponse.Status.OK, reservacion);
    }
        
    /**
     * Negociación de contenido. Aceptamos form-parameters para la creación de un nuevo recurso.
     * @param desde
     * @param hasta
     * @param cuarto
     * @param huesped
     * @return 
     */
//    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
//            produces = "application/json")
//    @ResponseStatus(HttpStatus.CREATED)
//    public ApiResponse guardarReservacionConFormData(final Date desde, final Date hasta, final Integer cuarto,final Integer huesped) 
//    {
//        Reservacion reservacion = new Reservacion(desde, hasta, cuarto,huesped);
//        this.oReservacionService.agregarReservacion(reservacion);
//        return new ApiResponse(ApiResponse.Status.OK, reservacion);
//    }
    
     @RequestMapping(value = "/{id}", method = RequestMethod.PUT,
            produces="application/json")
    public ApiResponse guardarReservacion(@PathVariable("id") final int id, 
            @RequestBody final Reservacion reservacionActualizado) 
    {
        final Reservacion reservacion = new Reservacion(id,
                reservacionActualizado.getDesde(), 
                reservacionActualizado.getHasta(),
                reservacionActualizado.getCuarto(),
                reservacionActualizado.getHuesped());
        this.oReservacionService.agregarReservacion(reservacion);
        return new ApiResponse(ApiResponse.Status.OK, reservacion);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE,
            produces="application/json")
    public ApiResponse eliminarReservacion(@PathVariable("id") final int id) 
    {
        final Reservacion reservacion = this.oReservacionService.obtenerReservacion(id);
        this.oReservacionService.eliminarReservacion(reservacion.getId());
        return new ApiResponse(ApiResponse.Status.OK,null);
    }
}
