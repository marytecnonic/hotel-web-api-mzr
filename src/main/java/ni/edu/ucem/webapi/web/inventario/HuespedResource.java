/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.web.inventario;

import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.core.ListApiResponse;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.service.HuespedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mzrodriguez
 */
@RestController
@RequestMapping("/v1/huesped")
public class HuespedResource {
    
     private final HuespedService oHuespedService;
    
    @Autowired
    public HuespedResource(final HuespedService oHuespedService)
    {
        this.oHuespedService = oHuespedService;
    }
    
    
    @RequestMapping(method = RequestMethod.GET, produces="application/json")
    public ListApiResponse<Huesped> obtenerHuesped(
            @RequestParam(value = "offset", required = false, defaultValue ="0") final Integer offset,
            @RequestParam(value = "limit", required = false, defaultValue="0") final Integer limit)
    {
        final Filtro paginacion = new Filtro.Builder()
                .paginacion(offset, limit)
                .build();

        Pagina<Huesped> huesped;
        huesped = this.oHuespedService.obtenerTodosHuesped(paginacion);
        
        return new ListApiResponse<Huesped>(ApiResponse.Status.OK, huesped);
    }
    
     @RequestMapping(method = RequestMethod.POST,
            produces = "application/json")
    public ApiResponse guardarHuesped(@RequestBody final Huesped pHuesped) 
    {
        this.oHuespedService.agregarHuesped(pHuesped);
        return new ApiResponse(ApiResponse.Status.OK, pHuesped);
    }
    
    
}
