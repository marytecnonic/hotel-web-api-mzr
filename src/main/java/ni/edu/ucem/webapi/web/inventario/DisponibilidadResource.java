/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.web.inventario;

import java.util.Date;
import java.util.Optional;
import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.service.ReservacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mzrodriguez
 */
@RestController
@RequestMapping("/v1/disponibilidad/cupos")
public class DisponibilidadResource {
    
    private final ReservacionService reservacionService;

    @Autowired
    public DisponibilidadResource(final ReservacionService reservacionService){
        this.reservacionService = reservacionService;
    }
    
    @RequestMapping(method = RequestMethod.GET, produces="application/json")
    public Cupo obtenerCupo(@RequestParam(required = true)
                            @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                            final Date fechaIngreso,
                            @RequestParam(required = true)
                            @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                            final Date fechaSalida,
                            Integer categoriaId,
                            Integer offset,
                            Integer limit){
        return this.reservacionService
                   .obtenerDisponiblidadCupo(fechaIngreso,
                                             fechaSalida,
                                             categoriaId,
                                             offset,
                                             limit);
    }
    
}
