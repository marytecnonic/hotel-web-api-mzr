/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.modelo;

import java.util.Date;
import java.util.List;

/**
 *
 * @author mzrodriguez
 */
public class Cupo {
    
    private Date fechaIngreso;
    private Date fechaSalida;
    private List<Cuarto> litaCuarto;
    
    public Cupo(){
    }
    
    public Cupo(Date fechaIngreso,Date fechaSalida, List<Cuarto> listaCuarto){
        this.fechaIngreso = fechaIngreso;
        this.fechaSalida = fechaSalida;
        this.litaCuarto = listaCuarto;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public List<Cuarto> getLitaCuarto() {
        return litaCuarto;
    }

    public void setLitaCuarto(List<Cuarto> litaCuarto) {
        this.litaCuarto = litaCuarto;
    }
      
      
    
}
