/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.modelo;

import java.math.BigInteger;

/**
 *
 * @author mzrodriguez
 */
public class Huesped {
    
    private Integer id;
    private String nombre;
    private String email;
    private String telefono;
    
    public Huesped(){
    }
    
    public Huesped(final Integer id,final String nombre,final String email,final String telefono){
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;     
    }
    
     public Huesped(final String nombre,final String email,final String telefono){
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;     
    }
   
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

  
        
}
