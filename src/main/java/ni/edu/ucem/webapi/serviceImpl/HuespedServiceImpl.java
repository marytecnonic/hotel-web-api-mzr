/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import ni.edu.ucem.webapi.dao.HuespedDAO;
import ni.edu.ucem.webapi.modelo.CategoriaCuarto;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.service.HuespedService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author mzrodriguez
 */
@Service
public class HuespedServiceImpl implements HuespedService{
    
     private final HuespedDAO oHuespedDAO;
    
     public HuespedServiceImpl(final HuespedDAO oHuespedDAO)
    {
        this.oHuespedDAO = oHuespedDAO;
    }
     
    /**
     *
     * @param paginacion
     * @return
     */
    @Override
    public Pagina<Huesped> obtenerTodosHuesped(final Filtro paginacion)
    {
        List<Huesped> huesped;
        final int count = this.oHuespedDAO.contar();
        if(count > 0)
        {
            huesped = this.oHuespedDAO.obtenerTodos(paginacion.getOffset(),
                    paginacion.getLimit());
        }
        else
        {
            huesped = new ArrayList<Huesped>();
        }
        return new Pagina<Huesped>(huesped, count,  paginacion.getOffset(), paginacion.getLimit());
    }
    
    @Transactional
    @Override
    public void agregarHuesped(final Huesped pHuesped) 
    {
        this.oHuespedDAO.agregar(pHuesped);
    }
    
}
