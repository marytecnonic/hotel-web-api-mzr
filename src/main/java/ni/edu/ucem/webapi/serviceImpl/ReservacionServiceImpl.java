/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import ni.edu.ucem.webapi.dao.CategoriaCuartoDAO;
import ni.edu.ucem.webapi.dao.CuartoDAO;
import ni.edu.ucem.webapi.dao.ReservacionDAO;
import ni.edu.ucem.webapi.modelo.CategoriaCuarto;
import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.service.ReservacionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author mzrodriguez
 */
@Service
public class ReservacionServiceImpl implements ReservacionService {
    
    private final ReservacionDAO oReservacionDAO;
    
     public ReservacionServiceImpl(final ReservacionDAO oReservacionDAO)
    {
        this.oReservacionDAO = oReservacionDAO;
    }
     
    @Transactional
    @Override
     public void agregarReservacion(final Reservacion pReservacion)
    {
        this.oReservacionDAO.agregar(pReservacion);
    }
     
    @Transactional
    @Override
    public void guardarReservacion(final Reservacion pReservacion)
    {
        if(pReservacion.getId() < 1)
        {
            throw new IllegalArgumentException("La reservacion del cuarto no existe");
        }
        this.oReservacionDAO.guardar(pReservacion);
    }
    
     @Transactional
    @Override
   public void eliminarReservacion(final int pId)
    {
        if(pId < 1)
        {
            throw new IllegalArgumentException("ID invalido. Debe ser mayor a cero");
        }
        this.oReservacionDAO.eliminar(pId);
    }
   
   @Override
   public Reservacion obtenerReservacion(final int pId)
    {
        return this.oReservacionDAO.obtenerPorId(pId);
    }
    
    @Override
    public Pagina<Reservacion> obtenerTodosReservacion(final Filtro paginacion)
    {
        List<Reservacion> reservacion;
        final int count = this.oReservacionDAO.contar();
        if(count > 0)
        {
            reservacion = this.oReservacionDAO.obtenerTodos(paginacion.getOffset(),
                    paginacion.getLimit());
        }
        else
        {
            reservacion = new ArrayList<Reservacion>();
        }
        return new Pagina<Reservacion>(reservacion, count,  paginacion.getOffset(), paginacion.getLimit());
    }
    
     @Transactional
    @Override
    public Cupo obtenerDisponiblidadCupo(final Date fechaIngreso,
                                         final Date fechaSalida,
                                         final Integer categoriaId,
                                         final Integer offset,
                                         final Integer limit){
        
        if(fechaIngreso.after(fechaSalida)){
            throw new IllegalArgumentException("La fecha de ingreso no puede ser mayor a la fecha salida.");
        }

        return this.oReservacionDAO
                   .obtenerDisponiblidadCupo(
                                           fechaIngreso,
                                           fechaSalida,
                                           categoriaId,
                                           offset,
                                           limit);
    }
    
}
