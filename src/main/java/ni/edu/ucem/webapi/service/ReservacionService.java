/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.service;

import java.util.Date;
import java.util.Optional;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Reservacion;

/**
 *
 * @author mzrodriguez
 */
public interface ReservacionService  {
    
     public Pagina<Reservacion> obtenerTodosReservacion(final Filtro paginacion);
     
    public void agregarReservacion(final Reservacion pReservacion);

    public void guardarReservacion(final Reservacion pReservacion);

    public void eliminarReservacion(final int pReservacion);

    public Reservacion obtenerReservacion(final int pReservacion);
    
     public Cupo obtenerDisponiblidadCupo(Date fechaIngreso,Date fechaSalida,Integer categoriaId,Integer offset,Integer limit);
    
}
