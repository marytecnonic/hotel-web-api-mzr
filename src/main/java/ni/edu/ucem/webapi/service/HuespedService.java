/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.service;

import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Reservacion;

/**
 *
 * @author mzrodriguez
 */
public interface HuespedService {
    
    /**
     *
     * @param paginacion
     * @return
     */
    public Pagina<Huesped> obtenerTodosHuesped(final Filtro paginacion);
    
    public void agregarHuesped(final Huesped pHuesped);
    
}
