/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.daoImpl;

import java.util.List;
import ni.edu.ucem.webapi.dao.HuespedDAO;
import ni.edu.ucem.webapi.modelo.Huesped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author mzrodriguez
 */
@Repository
public class HuespedDAOImpl implements HuespedDAO {
    
    private final JdbcTemplate jdbcTemplate;
   
    @Autowired
    public HuespedDAOImpl(final JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }
    
     @Override
    public Huesped obtenerPorId(final int pId) 
    {
        String sql = "select * from huesped where id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{pId}, 
                new BeanPropertyRowMapper<Huesped>(Huesped.class));
    }
   
    @Override
    public int contar()
    {
        final String sql = "select count(*) from huesped";
        return this.jdbcTemplate.queryForObject(sql, Integer.class);
    }

    @Override
    public List<Huesped> obtenerTodos(final int pOffset, final int pLimit) 
    {
        String sql = "select * from huesped offset ? limit ?";
        return this.jdbcTemplate.query(sql, new Object[]{pOffset, pLimit},
                new BeanPropertyRowMapper<Huesped>(Huesped.class));
    }

    @Override
    public void agregar(final Huesped pHuesped) 
    {
        final String sql = new StringBuilder()
                .append("INSERT INTO huesped")
                .append(" ")
                .append("(nombre, email, telefono)")
                .append(" ")
                .append("VALUES (?, ?, ?)")
                .toString();
        final Object[] parametros = new Object[4];
        parametros[0] = pHuesped.getNombre();
        parametros[1] = pHuesped.getEmail();
        parametros[2] = pHuesped.getTelefono();
        this.jdbcTemplate.update(sql,parametros);
        
    }

    @Override
    public void guardar(final Huesped pHuesped) 
    {        
       final String sql = new StringBuilder()
                .append("UPDATE huesped")
                .append(" ")
                .append("set nombre = ?")
                .append(",email = ?")
                .append(",telofono = ?")
                .append(" ")
                .append("where id = ?")
                .toString();
        final Object[] parametros = new Object[4];
        parametros[0] = pHuesped.getNombre();
        parametros[1] = pHuesped.getEmail();
        parametros[2] = pHuesped.getTelefono();
        parametros[3] = pHuesped.getId();
        this.jdbcTemplate.update(sql,parametros);
    }

    @Override
    public void eliminar(final int pId) 
    {
        final String sql = "delete from huesped where id = ?";
        this.jdbcTemplate.update(sql, new Object[]{pId});
    }

    
}
