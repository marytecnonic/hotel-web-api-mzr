/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.daoImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import ni.edu.ucem.webapi.dao.ReservacionDAO;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Reservacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author mzrodriguez
 */
@Repository
public class ReservacionDAOImpl implements ReservacionDAO {
    
    private final JdbcTemplate jdbcTemplate;
   
    @Autowired
    public ReservacionDAOImpl(final JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    public class ReservacionRowMapper implements RowMapper<Reservacion>{
        @Override
        public Reservacion mapRow(ResultSet rs, int rowNum) throws SQLException {
            Cuarto cuarto = new Cuarto();
            cuarto.setId(rs.getInt("cuartoId"));
            cuarto.setNumero(rs.getShort("numero"));
            cuarto.setDescripcion(rs.getString("descripcion"));
            cuarto.setCategoria(rs.getInt("categoria"));

            Huesped huesped = new Huesped();
            huesped.setId(rs.getInt("huespedId"));
            huesped.setNombre(rs.getString("nombre"));
            huesped.setTelefono(rs.getString("telefono"));
            huesped.setEmail(rs.getString("email"));

            Reservacion reservacion = new Reservacion();
            reservacion.setId(rs.getInt("reservacionId"));
            reservacion.setDesde(rs.getDate("desde"));
            reservacion.setHasta(rs.getDate("hasta"));
            reservacion.setCuarto(cuarto);
            reservacion.setHuesped(huesped);

            return reservacion;
        }
    }
    
    @Override
    public Reservacion obtenerPorId(final int pId) 
    {
         StringBuilder sql = new StringBuilder();
        sql.append("SELECT ");
        sql.append("reservacion.id as reservacionId, ");
        sql.append("reservacion.desde as desde, ");
        sql.append("reservacion.hasta as hasta, ");
        sql.append("reservacion.cuarto as cuartoId, ");
        sql.append("reservacion.huesped as huespedId,");
        sql.append("cuarto.id as cuartoId, ");
        sql.append("cuarto.descripcion as descripcion, ");
        sql.append("cuarto.categoria as categoria, ");
        sql.append("cuarto.numero as numero, ");
        sql.append("huesped.id as huespedId, ");
        sql.append("huesped.nombre as nombre, ");
        sql.append("huesped.email as email, ");
        sql.append("huesped.telefono as telefono ");
        sql.append("FROM reservacion ");
        sql.append("INNER JOIN cuarto ON ");
        sql.append("reservacion.cuarto = cuarto.id ");
        sql.append("INNER JOIN huesped ON ");
        sql.append("reservacion.huesped = huesped.id ");
        sql.append("WHERE reservacion.id = ?");

        return jdbcTemplate.queryForObject(sql.toString(), new Object[]{pId}, 
               new ReservacionRowMapper());
    }
   
    @Override
    public int contar()
    {
        final String sql = "select count(*) from reservacion";
        return this.jdbcTemplate.queryForObject(sql, Integer.class);
    }
    
     @Override
    public List<Reservacion> obtenerTodos(final int pOffset, final int pLimit) 
    {
        String sql = "select * from reservacion offset ? limit ?";
        return this.jdbcTemplate.query(sql, new Object[]{pOffset, pLimit},
                new BeanPropertyRowMapper<Reservacion>(Reservacion.class));
    }
    
    
    @Override
    public void agregar(final Reservacion pReservacion) 
    {
        //verificar si ya esxiste un reservacion
        String verificacion = "select * from reservacion where desde = ? and hasta = ? and cuarto = ? ";
        List<Reservacion> lista=  this.jdbcTemplate.query(verificacion,new Object[]{pReservacion.getDesde(), pReservacion.getHasta(),pReservacion.getCuarto()},
                new BeanPropertyRowMapper<Reservacion>(Reservacion.class));
        if(lista.size() != 0){
            return;
        } else{
            final String sql = new StringBuilder()
                    .append("INSERT INTO reservacion")
                    .append(" ")
                    .append("(desde, hasta, cuarto,huesped)")
                    .append(" ")
                    .append("VALUES (?, ?, ?, ?)")
                    .toString();
            final Object[] parametros = new Object[3];
            parametros[0] = pReservacion.getDesde();
            parametros[1] = pReservacion.getHasta();
            parametros[2] = pReservacion.getCuarto();
            parametros[3] = pReservacion.getHuesped();
            this.jdbcTemplate.update(sql,parametros);
        }
         
    }

    @Override
    public void guardar(final Reservacion pReservacion) 
    {       
         //verificar si ya esxiste un reservacion
        String verificacion = "select * from reservacion where desde = ? and hasta = ? and cuarto = ?";
        List<Reservacion> lista=  this.jdbcTemplate.query(verificacion,new Object[]{pReservacion.getDesde(), pReservacion.getHasta(),pReservacion.getCuarto()},
                new BeanPropertyRowMapper<Reservacion>(Reservacion.class));
        if(lista.size() != 0){
            return;
        } else{
                final String sql = new StringBuilder()
                         .append("UPDATE reservacion")
                         .append(" ")
                         .append("set desde = ?")
                         .append(",hasta = ?")
                         .append(",cuarto = ?")
                         .append(",huesped = ?")
                         .append(" ")
                         .append("where id = ?")
                         .toString();
                 final Object[] parametros = new Object[5];
                 parametros[0] = pReservacion.getDesde();
                 parametros[1] = pReservacion.getHasta();
                 parametros[2] = pReservacion.getCuarto();
                 parametros[3] = pReservacion.getHuesped();
                 parametros[4] = pReservacion.getId();
                 this.jdbcTemplate.update(sql,parametros);
        }
    }

    @Override
    public void eliminar(final int pId) 
    {
        final String sql = "delete from reservacion where id = ?";
        this.jdbcTemplate.update(sql, new Object[]{pId});
    }
    
    @Override
    public Cupo obtenerDisponiblidadCupo(final Date fechaIngreso,
                                         final Date fechaSalida,
                                         final Integer categoriaId,
                                         final Integer offset,
                                         final Integer limit) {

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM cuarto ");
        sql.append("WHERE id NOT IN ( ");
        sql.append(" SELECT cuarto FROM reservacion");
        sql.append(" WHERE (desde <= ? OR hasta <= :f?)");
        sql.append(" OR (desde <= ? OR hasta <= ?)");
        sql.append(")");

        if(categoriaId != null){
            sql.append("AND categoria = ?");
        }
      
        List<Cuarto> cuartosDisponibles = this.jdbcTemplate.query(
                                            sql.toString(),
                                           new Object[]{fechaIngreso, fechaIngreso,fechaSalida,fechaSalida,categoriaId},
                                            new BeanPropertyRowMapper<Cuarto>(Cuarto.class));
       
        return new Cupo(fechaIngreso, fechaSalida, cuartosDisponibles);
    }

  
}
