package ni.edu.ucem.webapi.dao;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

import ni.edu.ucem.webapi.modelo.CategoriaCuarto;

public interface CategoriaCuartoDAO 
{
    public CategoriaCuarto obtenerPorId(final int pId);

    public List<CategoriaCuarto> obtenerTodos(final int offset, final int limit);

    @PreAuthorize("hasRole('ADMIN')")
    public void agregar(final CategoriaCuarto pCategoriaCuarto);

    @PreAuthorize("hasRole('ADMIN')")
    public void guardar(final CategoriaCuarto pCategoriaCuarto);

    @PreAuthorize("hasRole('ADMIN')")
    public void eliminar(final int pId);
    
    public int contar();
}
