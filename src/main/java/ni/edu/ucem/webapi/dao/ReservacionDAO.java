/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.modelo.Reservacion;
import org.springframework.security.access.prepost.PreAuthorize;

/**
 *
 * @author mzrodriguez
 */
public interface ReservacionDAO {
    
    public Reservacion obtenerPorId(final int pId);

    public int contar();
    
    public List<Reservacion> obtenerTodos(final int offset, final int limit);

    @PreAuthorize("hasRole('ADMIN')")
    public void agregar(final Reservacion pReservacion);

    @PreAuthorize("hasRole('ADMIN')")
    public void guardar(final Reservacion pReservacion);

    @PreAuthorize("hasRole('ADMIN')")
    public void eliminar(final int pId);
    
      public Cupo obtenerDisponiblidadCupo(Date fechaIngreso,Date fechaSalida, Optional<Integer> categoriaId,Optional<Integer> offset,Optional<Integer> limit);
    
}
