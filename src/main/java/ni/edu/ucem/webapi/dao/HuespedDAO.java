/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.dao;

import java.util.List;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Reservacion;
import org.springframework.security.access.prepost.PreAuthorize;

/**
 *
 * @author mzrodriguez
 */
public interface HuespedDAO {
    
     public Huesped obtenerPorId(final int pId);

    public int contar();
    
    public List<Huesped> obtenerTodos(final int offset, final int limit);

    //@PreAuthorize("hasRole('ADMIN')")
    public void agregar(final Huesped pHuesped);

    @PreAuthorize("hasRole('ADMIN')")
    public void guardar(final Huesped pHuesped);

    @PreAuthorize("hasRole('ADMIN')")
    public void eliminar(final int pId);
    
    
}
